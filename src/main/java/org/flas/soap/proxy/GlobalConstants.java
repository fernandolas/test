package org.flas.soap.proxy;

/**
 * 
 * @author fernando.las@gmail.com
 *
 */
public class GlobalConstants {

	private GlobalConstants() {
	}

	public static final String CONTEXT_CACHE_KEY = "CONTEXT_CACHE_KEY";

}
